import tensorflow as tf

model =tf.keras.models.load_model(r"C:\Users\IMOE001\Desktop\cyberproject\mask_detector.model")

#Convert to tflite
converter = tf.lite.TFLiteConverter.from_keras_model(model)

#Implement optimization strategy for smaller model sizes
converter.optimizations = [tf.lite.Optimize.DEFAULT] #Uses default optimization strategy to reduce the model size
tflite_model = converter.convert()
open("lite_mask_detector_model.tflite", "wb").write(tflite_model)