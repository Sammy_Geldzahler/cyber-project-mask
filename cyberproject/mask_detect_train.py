# all imports - will refer to them when i get to them in code

from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.applications import MobileNetV2
from tensorflow.keras.layers import AveragePooling2D
from tensorflow.keras.layers import Dropout
from tensorflow.keras.layers import Flatten
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import Input
from tensorflow.keras.models import Model
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.applications.mobilenet_v2 import preprocess_input
from tensorflow.keras.preprocessing.image import img_to_array
from tensorflow.keras.preprocessing.image import load_img
from tensorflow.keras.utils import to_categorical
from sklearn.preprocessing import LabelBinarizer
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report
import numpy as np
import os

# variabels

# initial learning rate, epochs and batch size(all recomended for this amount of data)
INIT_LR = 1e-4
EPOCHS = 20
BS = 32



# directory to the dataset in computer
DIRECTORY = r"C:\Users\IMOE001\Desktop\cyberproject\dataset"
# the two files in dataset, they will also be the labels
CATEGORIES = ["with_mask", "without_mask"]


# creating to empty lists
# data is going to append all the pictures
# labels is going to append the corresponding category according wether it has a mask or not
data = []
labels = []


# loop in the categories list
for category in CATEGORIES:
    # using os which provides function for creating a directory and saving path of the path to the pictures in path
    path = os.path.join(DIRECTORY, category)

    # looping through every picture in current category
    for img in os.listdir(path):

        # with os, join the current path with the current image
        img_path = os.path.join(path, img)

        # using load image from keras which while make all pictures same size
        image = load_img(img_path, target_size=(224, 224))

        # also keras function to turn all images to arrays
        image = img_to_array(image)

        # need this for mobilenet training, more later...
        image = preprocess_input(image)

        # append the image to the data list created above and the corresponding cotegory to the labels list
        data.append(image)
        labels.append(category)


# changing the labels to binary for it to work using imported labelbinarizer
lb = LabelBinarizer()
labels = lb.fit_transform(labels)
labels = to_categorical(labels)

# converting the lists into numpy arrays with imported numpy
data = np.array(data, dtype="float32")
labels = np.array(labels)


# spliting the data and labels, 20 percent goes to testing the rest for training
# stratify so that the proportion is the same
# random_state to decide what splits
(trainX, testX, trainY, testY) = train_test_split(data, labels, test_size=0.20, stratify=labels, random_state=42)


# making more images from the pictures we have with imagedatagenerator

aug = ImageDataGenerator(
	rotation_range=20,
	zoom_range=0.15,
	width_shift_range=0.2,
	height_shift_range=0.2,
	shear_range=0.15,
	horizontal_flip=True,
	fill_mode="nearest")


# we need two models: the basemodel of mobilenet and later also the head model we need to create
# imagnet is pretrained for images
# not including top because we manually making one
# photosize with all colors chanels
baseModel = MobileNetV2(weights="imagenet", include_top=False, input_tensor=Input(shape=(224, 224, 3)))


# creating headmodel (with help of all inported tensorflow modules) with output of mobilenet
headModel = baseModel.output

# do pooling action on images so that it reduces the parameters to learn from
headModel = AveragePooling2D(pool_size=(7, 7))(headModel)

# flatten the mullti dimensional input to one dimention
headModel = Flatten(name="flatten")(headModel)

# 128 conection of relu becouse it is for images in the dense layer
headModel = Dense(128, activation="relu")(headModel)

# ignore a few neurons to prevent the model from overfitting
headModel = Dropout(0.5)(headModel)

# in dense layer get to types of labels it is softmax becouse of labels in binary
headModel = Dense(2, activation="softmax")(headModel)


# place the head model on top of the base model (this will become the actual model we will train)
model = Model(inputs=baseModel.input, outputs=headModel)

# loop over all layers in the base model and freeze them so they will not be updated during the first training process
for layer in baseModel.layers:
    layer.trainable = False


# compile our model
print("[INFO] compiling model...")
opt = Adam(lr=INIT_LR, decay=INIT_LR / EPOCHS)
model.compile(loss="binary_crossentropy", optimizer=opt, metrics=["accuracy"])

# train the head of the network
print("[INFO] training head...")
H = model.fit(
	aug.flow(trainX, trainY, batch_size=BS),
	steps_per_epoch=len(trainX) // BS,
	validation_data=(testX, testY),
	validation_steps=len(testX) // BS,
	epochs=EPOCHS)

# make predictions on the testing set
print("[INFO] evaluating network...")
predIdxs = model.predict(testX, batch_size=BS)

# for each image in the testing we need to find the index of the label with corresponding largest predicted probability
predIdxs = np.argmax(predIdxs, axis=1)

# show a nicely formatted classification report
print(classification_report(testY.argmax(axis=1), predIdxs, target_names=lb.classes_))

# serialize the model to disk
print("[INFO] saving mask detector model...")
model.save("mask_detector.model", save_format="h5")



