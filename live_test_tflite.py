from keras.preprocessing.image import img_to_array
from keras.preprocessing import image
import cv2
import numpy as np
import tflite_runtime.interpreter as tflite

face_classifier=cv2.CascadeClassifier('haarcascade_frontalface_default.xml')


mask_interpreter = tflite.Interpreter(model_path="lite_mask_detector_model.tflite")
mask_interpreter.allocate_tensors()

mask_input_details = mask_interpreter.get_input_details()
mask_output_details = mask_interpreter.get_output_details()

mask_input_shape = mask_input_details[0]['shape']

mask_labels = ['Mask', 'No mask']

vs =cv2.VideoCapture(0)


while True:
    #vs = cv2.VideoCapture(0, cv2.CAP_DSHOW)

    ret, frame = vs.read()



    labels=[]
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)





    faces = face_classifier.detectMultiScale(frame, 1.3, 5)
    for (x, y, w, h) in faces:
        cv2.rectangle(frame, (x, y), (x + w, y + h), (255, 0, 0), 2)
        roi_gray = gray[y:y + h, x:x + w]


        # Get image ready for prediction
        # Get image ready for prediction


        roi_color = frame[y:y + h, x:x + w]
        roi_color = cv2.resize(roi_color, (224, 224), interpolation=cv2.INTER_AREA)
        roi_color = np.array(roi_color).reshape( -1, 224, 224 ,3)  # input shape is (1, 200,200,3)
        roi_color = roi_color.astype(np.float32)

        mask_interpreter.set_tensor(mask_input_details[0]['index'], roi_color)
        mask_interpreter.invoke()
        mask_preds = mask_interpreter.get_tensor(mask_output_details[0]['index'])

        mask_predict = (mask_preds >= 0.5).astype(int)[:, 0]
        mask_label = mask_labels[mask_predict[0]]
        mask_label_position = (x, y + h + 50)  # 50 pixels below to move the label outside the face
        cv2.putText(frame, mask_label, mask_label_position, cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)

    cv2.imshow('mask Detector', frame)
    if cv2.waitKey(1) & 0xFF == ord('q'):  # Press q to exit
        break
vs.release()
cv2.destroyAllWindows()

